<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 16/03/2019
 * Time: 13:32
 */

namespace App\Http\Controllers\AutoPhone;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;


class UploudController extends Controller
{
    public function index()
    {
        return view('phone.index');
    }

    public function upload(Request $request){
        $file = Input::file('file');
        $file_name = $file->getClientOriginalName();
        $file->move('file', $file_name);
        $resould = \Maatwebsite\Excel\Facades\Excel::read('file/'.$file_name, function ($reader){
            $reader->all();
        })->get();
        dd($resould);
    }

}