<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Migration

Writing Migrations

 - php artisan make:migration create_users_table
 - php artisan make:migration add_votes_to_users_table --table=users
 
 Running Migrations
 
 - php artisan migrate
 - php artisan migrate --force
 - php artisan migrate:rollback
 

## Seeder

Writing Seeders

- php artisan make:seeder UsersTableSeeder
- php artisan db:seed
- php artisan db:seed --class=UsersTableSeeder
- php artisan migrate:refresh --seed 


## Controller

Writing Migrations

- php artisan make:controller ShowProfile 
- php artisan make:controller Auth\\ShowProfile 
- php artisan make:controller PhotoController --resource
- php artisan make:controller PhotoController --resource --model=Photo

## Factory

Writing Migrations

- php artisan make:factory PostFactory

## Model

- php artisan make:model Flight