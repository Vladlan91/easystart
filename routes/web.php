<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');


Route::get('logout', function(){

    return back();

});

Route::post('logout', 'AuthController@logout');
Route::get('phone', 'AutoPhone\UploudController@index')->name('phone');
Route::put('upload', 'AutoPhone\UploudController@upload')->name('upload');

Route::get('export', 'MyController@export')->name('export');
Route::get('importExportView', 'MyController@importExportView');
Route::post('import', 'MyController@import')->name('import');

Route::get('news', 'NewsController@index')->name('news');

Auth::routes();


