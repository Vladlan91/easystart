<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <link href="{{ asset('css/bootstrap.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ asset('css/style.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ asset('css/fontawesome-all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/SidebarNav.min.css') }}" media='all' rel='stylesheet' type='text/css' />
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Yanone+Kaffeesatz:200,300,400,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"> </script>
</head>

<body class="cbp-spmenu-push">
<div class="main-content">
    <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
        <!--left-fixed -navigation-->
        <aside class="sidebar-left" style="    background-color: #a2a5aa;">
            <h1>
                <a href="{{route('home')}}" class="logo">
                    <img style="width: 148px;" src="images/logo.jpg" alt="">
                </a>
            </h1>
            <ul class="sidebar-menu">
                <li class="treeview active">
                    <a href="{{route('news')}}">
                        <i class="fas fa-home"></i>
                        <span>Новини</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fas fa-info"></i>
                        <span>Про проект</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fab fa-servicestack"></i>
                        <span>Наші послуги</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fab fa-buromobelexperte"></i>
                        <span>Наші проекти</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fas fa-plus"></i>
                        <span>Більше</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="#">
                                <i class="fa fa-angle-right"></i>Курси</a>
                        </li>
                        <li>
                            <a href="{{route('phone')}}">
                                <i class="fa fa-angle-right"></i>Проекти</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-angle-right"></i>Стартапи</a>
                        </li>

                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fas fa-address-book"></i>
                        <span>Контакти</span>
                    </a>
                </li>
                @guest
                    <li><a href="{{ route('login') }}">Увійти</a></li>
                    <li><a href="{{ route('register') }}">Зареєструватись</a></li>
                @else
                    <li class="treeview">
                        <a href="#">
                            <span><img src="{{asset('images/avatar.jpg')}}" style="width: 27px; height: 27px; margin-left: -15px; border-radius: 20px; margin-right: 4px; float: left" alt="">{{ Auth::user()->name }}</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            @if(Auth::user())
                                <li>
                                    <a href="">
                                        Адмін-панель
                                    </a>
                                </li>
                            @endif
                                <li>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-info btn-lg btn-block  w3ls-btn p-1 text-uppercase font-weight-bold"
                                                aria-pressed="false">
                                            Вийти
                                        </button>
                                    </form>
                                </li>
                        </ul>
                    </li>
                @endguest
            </ul>
        </aside>
    </div>
</div>

<header class="header-section">
    <div class="header-left  clearfix">
        <!--logo start-->
        <div class="brand">
            <button id="showLeftPush">
            </button>
        </div>
        <!--logo end-->
    </div>
    <div class="header-right">
    </div>
</header>
<div id="page-wrapper">
    @yield('content')
<div class="footer py-md-5 pt-sm-3 pb-sm-5">
    <div class="container">
        <div class="row p-sm-5 px-3 py-5">
            <!-- footer grid top -->
            <div class="col-lg-8">
                <div class="footer-top">
                    <h2>
                        <img style="border-radius: 50px; width: 90px; height: 90px" src="{{asset('images/logo.png')}}" alt="">
                    </h2>
                    <p class="mt-2">Почніть робити те, що потрібно. Потім робіть те, що можливо. І ви раптом виявите, що робите неможливе. – Св.Франциск Асізський
                    </p>
                </div>
                <!-- //footer grid top -->
                <!-- footer-bottom -->
                <div class="footer-bottom pt-5">
                    <div class="row">
                        <!-- footer grid1 -->
                        <div class="col-sm-4 footv3-left">
                            <h3 class="mb-3 w3f_title">Навігація по сайту</h3>
                            <ul class="list-agileits">
                                <li>
                                    <a href="index.html">
                                        Home
                                    </a>
                                </li>
                                <li class="my-3">
                                    <a href="about.html">
                                        About Us
                                    </a>
                                </li>
                                <li class="mb-3">
                                    <a href="services.html">
                                        Services
                                    </a>
                                </li>
                                <li>
                                    <a href="contact.html">
                                        Contact Us
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- //footer grid1 -->
                        <!-- footer grid2 -->
                        <div class="col-sm-4  footv3-left my-sm-0 my-5">
                            <h3 class="mb-3 w3f_title">Цікаві посилання</h3>
                            <ul class="list-agileits">
                                <li>
                                    <a href="index.html">
                                        Link One
                                    </a>
                                </li>
                                <li class="my-3">
                                    <a href="about.html">
                                        Link Two
                                    </a>
                                </li>
                                <li class="mb-3">
                                    <a href="services.html">
                                        Link Three
                                    </a>
                                </li>
                                <li>
                                    <a href="contact.html">
                                        Link Four
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- //footer grid2 -->
                        <!-- footer grid3 -->
                        <div class="col-sm-4 footv3-left">
                            <h3 class="w3f_title mb-3">Доступні навчання</h3>
                            <ul class="list-agileits">
                                <li>
                                    <a href="index.html">
                                        Link One
                                    </a>
                                </li>
                                <li class="my-3">
                                    <a href="about.html">
                                        Link Two
                                    </a>
                                </li>
                                <li class="mb-3">
                                    <a href="services.html">
                                        Link Three
                                    </a>
                                </li>
                                <li>
                                    <a href="contact.html">
                                        Link Four
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- //footer grid3 -->
                    </div>
                </div>
                <!-- //footer bottom -->
            </div>
            <!-- //footer bottom -->
            <!-- footer right -->
            <div class="col-lg-4 mt-lg-0 mt-5">
                <h3 class="mb-3 w3f_title">Контакти</h3>
                <div class="fv3-contact">
                    <span class="fas fa-envelope-open mr-2"></span>
                    <p>
                        <a href="mailto:example@email.com">info@example.com</a>
                    </p>
                </div>
                <div class="fv3-contact my-3">
                    <span class="fas fa-phone-volume mr-2"></span>
                    <p>+456 123 7890</p>
                </div>
                <div class="footerv2-w3ls pt-4">
                    <h3 class="w3f_title">Ми в соціальних мережах</h3>
                    <ul class="social-iconsv2 agileinfo pt-3">
                        <li  style="border-radius: 20px;" >
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li  style="border-radius: 20px;" >
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li  style="border-radius: 20px;" >
                            <a href="#">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                        </li>
                        <li  style="border-radius: 20px;" >
                            <a href="#">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- //footer right -->
        </div>
        <!-- //footer row -->
    </div>

</div>
</div>
</body>
<script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
<script src="{{ asset('js/classie.js') }}"></script>
<script>
    var menuLeft = document.getElementById('cbp-spmenu-s1'),
        showLeftPush = document.getElementById('showLeftPush'),
        body = document.body;

    showLeftPush.onclick = function () {
        classie.toggle(this, 'active');
        classie.toggle(body, 'cbp-spmenu-push-toright');
        classie.toggle(menuLeft, 'cbp-spmenu-open');
        disableOther('showLeftPush');
    };


    function disableOther(button) {
        if (button !== 'showLeftPush') {
            classie.toggle(showLeftPush, 'disabled');
        }
    }
</script>
<script src="{{ asset('js/SidebarNav.min.js') }}"></script>
    <script>
    $('.sidebar-menu').SidebarNav()
    </script>
    <script>
        window.onload = function () {
            document.getElementById("password1").onchange = validatePassword;
            document.getElementById("password2").onchange = validatePassword;
        }

        function validatePassword() {
            var pass2 = document.getElementById("password2").value;
            var pass1 = document.getElementById("password1").value;
            if (pass1 != pass2)
                document.getElementById("password2").setCustomValidity("Passwords Don't Match");
            else
                document.getElementById("password2").setCustomValidity('');
            //empty string means no validation error
        }
    </script>
<script src="{{ asset('js/move-top.js') }}"></script>
<script src="{{ asset('js/easing.js') }}"></script>
<script>
    jQuery(document).ready(function ($) {
        $(".scroll ").click(function (event) {
            event.preventDefault();

            $('html,body').animate({
                scrollTop: $(this.hash).offset().top
            }, 1000);
        });
    });
</script>
<script>
    $(document).ready(function () {
        /*
         var defaults = {
             containerID: 'toTop', // fading element id
             containerHoverID: 'toTopHover', // fading element hover id
             scrollSpeed: 1200,
             easingType: 'linear'
         };
         */

        $().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
</script>
<script src="{{ asset('js/counter.js') }}"></script>
<script src="{{ asset('js/SmoothScroll.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>
</body>
</html>
