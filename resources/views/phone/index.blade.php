@extends('layouts.app')

@section('content')
    <div class="page-container">
        <img src="{{asset('/images/ban55.jpg')}}" alt="" style="position: relative;">
        <div class="bd-example" id="cart">
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLabel">Створення аудио виклику</h4>
                        </div>
                        <div class="modal-body">
                            <form action="action_page.php" method="POST">
                                <div class="form-group">
                                    <label for="message-text" class="form-control-label">Номер телефону:</label>
                                    <input type="text" name="myValue" id="myValue" value="">
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                            <button type="submit"  class="btn btn-primary">Створити</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="bd-example" id="cart">
            <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLabel">Завантажити файл</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('import')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="message-text" class="form-control-label">Номер телефону:</label>
                                    <input type="file" name="file" id="file" value="">
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                            <button type="submit"  class="btn btn-primary">Створити</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="main" style="">
            <button type="button" data-toggle="modal" data-whatever="@getbootstrap" style="font-size: 14px; padding: 10px; padding-left: 20px; position: absolute; top:20%; padding-right: 20px;  font-weight: 800;color: #fff" data-target="#exampleModal">Внести телефон</button>
        </div>
    </div>
    <button type="button" data-toggle="modal" data-whatever="@getbootstrap" style="font-size: 14px; padding: 10px; padding-left: 20px; position: absolute; left: 30%; top:20%; padding-right: 20px;  font-weight: 800;color: #fff" data-target="#exampleModal2">Загрузити таблицю</button>
@endsection