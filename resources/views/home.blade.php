@extends('layouts.app')

@section('content')
        <div class="banner">
            <div class="banner_text_agile">
                <!-- Carousel -->
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active text-center">
                            <img style="border-radius: 50px; width: 90px; height: 90px" src="{{asset('images/logo.png')}}" alt="">
                            <h3 class="b-w3ltxt text-capitalize mt-4">розвивай свій бізнес з нами</h3>
                            <p class="mx-auto text-capitalize mt-2">Як розпочати свій бізнес, як зробити його унікальним, як привернути увагу суспільства...</p>
                            <a class="btn btn-banner mt-md-3 mt-2 text-capitalize" href="#" role="button">дізнатись більше</a>
                        </div>
                        <!-- slider text -->
                        <div class="carousel-item text-center">
                            <img style="border-radius: 50px; width: 90px; height: 90px" src="{{asset('images/logo.png')}}" alt="">
                            <h3 class="b-w3ltxt text-capitalize mt-4">втілюємо ідеї в реальність</h3>
                            <p class="mx-auto text-capitalize mt-2">У вас з'явилася блискуча ідея? Поспішайте! У вас є лише 72 години!</p>
                            <a class="btn btn-banner mt-md-3 mt-2 text-capitalize" href="#" role="button">дізнатись більше</a>
                        </div>
                        <!-- slider text -->
                        <div class="carousel-item text-center">
                            <img style="border-radius: 50px; width: 90px; height: 90px" src="{{asset('images/logo.png')}}" alt="">
                            <h3 class="b-w3ltxt text-capitalize mt-4">навчаємо проектні команди</h3>
                            <p class="mx-auto text-capitalize mt-2">Команда робить те, що не під силу одній людині</p>
                            <a class="btn btn-banner mt-md-3 mt-2 text-capitalize" href="#" role="button">дізнатись більше</a>
                        </div>
                        <!-- slider text -->
                    </div>
                </div>
                <!-- Carousel -->
            </div>


        </div>
        <!-- //banner -->
        <!-- about -->
        <section class="wthree-row py-sm-5 py-3">
            <div class="container py-md-5">
                <div class="py-lg-5 py-3 bg-pricemain text-center">
                    <h3 class="agile-title text-uppercase">чим ми займаємося</h3>
                    <span class="w3-line"></span>
                </div>
                <div class="row py-lg-5 pt-md-5 pt-3 d-flex justify-content-center">
                    <div class="card col-lg-3 col-md-6 border-0">
                        <div class="card-body bg-light">
                            <div class="card-img-top pt-3">
                                <h5 class=" card-title">навчання</h5>
                            </div>
                            <p class="card-text mb-3 ">Якщо Ви мрієте про стабільно розвиваючий бізнес, Вам необхідний персонал, з підприємницькими здібностями, Ви зобов'язані просувати, розвивати персонал, який має відмінні якості і підприємницьку хватку. Наша мета створити Вам команду, яка буде жити Вашим бізнесом.</p>
                        </div>
                        <img class="card-img-top" src="images/a1.png" alt="Card image cap">
                    </div>
                    <div class="card col-lg-3 col-md-6 border-0 mt-md-0 mt-5">
                        <img class="card-img-top" src="images/a2.jpg " alt="Card image cap ">
                        <div class="card-body bg-light text-center">
                            <h5 class="card-title ">Розвиток проектів</h5>
                            <p class="card-text mb-3 ">Створення та ведення сторінок клієнта в соц. мережах (Facebook, Instagram, Google+, Twitter), створення унікального контенту, пошукова оптимізація сайту, організація заходів щодо збільшення замовлень,
                                пошук цільової аудиторії, інтеграція сайту з соціальними мережами.</p>
                            <a href="#ab-bot" class="btn scroll">Читати більше</a>
                        </div>
                    </div>
                    <div class="card col-lg-3 col-md-6 border-0 mt-lg-0 mt-5 ">
                        <img class="card-img-top " src="images/a3.jpg " alt="Card image cap ">
                        <div class="card-body bg-light text-center">
                            <h5 class="card-title ">Розробка сайтів</h5>
                            <p class="card-text mb-3 ">Ми надаємо широкий спектр послуг - від розробки сайтів-візиток і корпоративних сайтів до інтернет-магазинів. Все залежить від конкретних цілей, які ставить перед нами замовник і виділеного бюджету, також проводим юзабіліті аудит, аби Вашим клієнтам було комфортно з Вами.</p>
                            <a href="#ab-bot" class="btn scroll">Читати більше</a>
                        </div>
                    </div>
                    <div class="card col-lg-3 col-md-6 border-0 mt-lg-0 mt-5 text-right">
                        <div class="card-body bg-light">
                            <h5 class="card-title  pt-3">Пошук ідей </h5>
                            <p class="card-text mb-3 ">Окрім надання послуг в сфері IT та SMM маркетингу, ми в стабільному пошуку цікавих ідей та проектів, ми завжди відкриті, все що Вам залишається так це просто подати ідею на нашому сайті і ми обовязково з Вами зв'яжемось.</p>
                        </div>
                        <img class="card-img-top " src="images/a4.png " alt="Card image cap ">
                    </div>
                </div>
            </div>
        </section>
        <!-- //about -->
        <!-- about-bottom -->
        <section class="wthree-row py-sm-5 py-3" id="ab-bot">
            <div class="row justify-content-center align-items-center no-gutters abbot-main">
                <div class="col-lg-6 p-0">
                    <img src="images/s1.jpg" class="img-fluid" alt="" />
                </div>
                <div class="col-lg-6 p-0 abbot-right">
                    <div class="card">
                        <div class="card-body px-sm-5 py-5 px-4">
                            <h3 class="stat-title card-title align-self-center">чим ми унікальні?</h3>
                            <span class="w3-line"></span>
                            <p class="card-text align-self-center my-3">
                                Окрім надання послуг в сфері IT та SMM маркетингу, ми інтенсивно розвиваємо Власні проекти, в сфері будівництва, інтернет торгівлі, а також в сфері послуг.</p>
                            <p class="card-text align-self-center mb-4">                                Тому ми, як ніхто, розуміємо специфіку конкурентного ринку і розуміємо, як якість нашої роботи впливає на подальше існування проекту.</p>
                            <a href="#" class="btn btn-primary abt_card_btn bg-light">Читати більше</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row  align-items-center no-gutters abbot-grid2">
                <div class="col-lg-6 py-lg-3 px-lg-5 p-sm-5 px-3 py-5 abbot-right">
                    <h3 class="stat-title card-title align-self-center pt-3 text-center">Які таланти ми шукаємо?</h3>
                    <span class="w3-line mx-auto text-center d-block"></span>
                    <div class="progress_agile mx-auto mt-5">
                        <div class="progress-outer mt-3">
                            <div class="progress">
                                <div class="progress-bar progress-bar-info progress-bar-striped active" style="width:90%; box-shadow:-1px 10px 10px rgba(91, 192, 222, 0.7);"></div>
                                <div class="progress-value">90%</div>
                            </div>
                            <h6 class="text-right text-capitalize pt-3">підприємницькі здібності</h6>
                        </div>
                        <div class="progress-outer  my-4">
                            <div class="progress">
                                <div class="progress-bar progress-bar-warning progress-bar-striped active" style="width:80%; box-shadow:-1px 10px 10px rgba(240, 173, 78,0.7);"></div>
                                <div class="progress-value">80%</div>
                            </div>
                            <h6 class="text-right text-capitalize pt-3">навики SEO та SMM маркетингу </h6>
                        </div>
                        <div class="progress-outer">
                            <div class="progress">
                                <div class="progress-bar progress-bar-success progress-bar-striped active" style="width:70%; box-shadow:-1px 10px 10px rgba(116, 195, 116,0.7);"></div>
                                <div class="progress-value">70%</div>
                            </div>
                            <h6 class="text-right text-capitalize pt-3">навики програмування</h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 p-0">
                    <img src="images/b4.jpg" class="img-fluid" alt="" />
                </div>
            </div>
        </section>
        <!-- //about bottom -->
        <!-- stats -->
        <section class="agile_stats">
            <div class="container-fluid pt-5">
                <div class="row pt-lg-5 w3-abbottom">
                    <div class="col-lg-6 px-sm-5 px-3">
                        <div class="stats_agile mb-5">
                            <h3 class="stat-title text-uppercase">Ми дивимось в майбутнє</h3>
                            <span class="w3-line"></span>
                            <p class="mt-3">Наша мета це створення суспільства бізнесменів, приєднавшись до нашої команди і взявши участь хоча б в одному із наших проектів Ви дістанете неперевершений досвід власника бізнесу від першого лиця.
                                </p>
                        </div>
                        <div class="row">
                            <div class="counter col-4">
                                <i class="far fa-smile fa-2x"></i>
                                <div class="timer count-title count-number mt-2" data-to="2800" data-speed="1500"></div>
                                <p class="count-text text-capitalize">постійних клієнтів</p>
                            </div>

                            <div class="counter col-4 px-4">
                                <i class="fas fa-database fa-2x"></i>
                                <div class="timer count-title count-number mt-2" data-to="12" data-speed="1500"></div>
                                <p class="count-text text-capitalize">власних проектів</p>
                            </div>

                            <div class="counter col-4">
                                <i class="fab fa-slideshare fa-2x"></i>
                                <div class="timer count-title count-number mt-2" data-to="18" data-speed="1500"></div>
                                <p class="count-text text-capitalize">наша команда</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <img src="images/psd.png" class="img-fluid" alt="" />
                    </div>
                </div>
            </div>
        </section>
        <!-- //stats -->

        <!-- services bottom -->
        <div class="serv_bottom py-5">
            <div class="container py-sm-3">
                <div class="d-sm-flex justify-content-around pb-4">
                    <h4 class="agile-ser_bot text-capitalize text-white">В пошуках цікавих проектів?</h4>
                    <a href="portfolio.html" class="text-uppercase serv_link align-self-center bg-light btn px-sm-4 px-2">переглянути</a>
                </div>
                <hr>
                <h5 class="text-center text-uppercase text-white pt-4">everything you need is here</h5>
            </div>
        </div>
        <!-- //services bottom -->
        <!-- slide -->
        <section class="wthree-row py-sm-5 py-3 slide-bg">
            <div class="container py-md-5 py-3">
                <div class="p-lg-5 bg-pricemain">
                    <h3 class="agile-title text-uppercase text-white">Стань частиною нашої команди</h3>
                    <span class="w3-line"></span>
                    <h5 class="agile-title text-capitalize pt-4">Ми допомагаємо молодому поколінню реалізувати себе в Україні, а не за її межами!</h5>
                    <p class="text-light py-4">

                    </p>
                    <a href="services.html" class="text-uppercase serv_link align-self-center bg-light btn px-4">подати заявку</a>
                </div>
            </div>
        </section>
@endsection
